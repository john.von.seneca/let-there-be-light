
Run the following command
```bash
sudo vim /etc/default/hddtemp
```

Add the following lines
```bash
RUN_DAEMON="true"
DISKS="/dev/sda | /dev/sdb | /dev/sdc | /dev/sdd"
```


hddtemp doesnt recognize ssd's

* Run the following command  
  ```bash
  sudo smartctl /dev/sdb -a | grep -i Temp
  ```

* Output: 190 Temperature_Celsius     0x0022   111   104   000    Old_age   Always           -       36

* 190 is the id which needs to be added to /etc/hddtemp.db
* ```bash
  sudo cp /etc/hddtemp.db /etc/hddtemp.db.bkp
  sudo echo "Samsung SSD 850 EVO 500G B" 190 C "ssd-500" >> /etc/hddtemp.db
  ```



