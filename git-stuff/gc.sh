cd ~/repos

# echo "git repo: $1"
if [[ $1 == https* ]];
then
	dirgitcurr=`echo $1 | sed 's/https:\/\/git[^\/]*\/\(.*\)\/\(.*\).git\{0,1\}/\2\/\1/'`
fi
if [[ $1 == git* ]];
then
	dirgitcurr=`echo $1 | sed 's/.*:\(.*\)\/\(.*\)\(.git\)\{0,1\}/\2\/\1/'`
fi

# echo $dirgitcurr

if [[ -z $dirgitcurr ]];
then
	echo "dirgitcurr not set"
	exit 125
fi

depth=inf
if [[ "$#" -gt 2 ]];
then
	echo "got 2 args, setting depth"
	depth=$2
fi

echo "sub-directory: $dirgitcurr depth: $depth"

if [[ $depth == inf ]];
then
	git clone $1 ~/repos/$dirgitcurr
else
	git clone --depth 1 $1 ~/repos/$dirgitcurr
fi
	

# sed 's/https:\/\/github.com\/\(.*\).git/\1/'
